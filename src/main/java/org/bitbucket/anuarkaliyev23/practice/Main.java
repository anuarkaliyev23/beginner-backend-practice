package org.bitbucket.anuarkaliyev23.practice;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import io.javalin.Javalin;
import org.bitbucket.anuarkaliyev23.practice.configuration.DatabaseConfiguration;
import org.bitbucket.anuarkaliyev23.practice.controllers.EmployeeController;
import org.bitbucket.anuarkaliyev23.practice.model.Employee;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException {
        Javalin app = Javalin.create();
        DatabaseConfiguration databaseConfiguration = new DatabaseConfiguration("jdbc:sqlite::memory:");
        ConnectionSource source = databaseConfiguration.connectionSource();
        databaseConfiguration.createTables();
        EmployeeController employeeController = new EmployeeController(DaoManager.createDao(source, Employee.class));

        app.get("/employee", employeeController::list);
//        app.get("/employee/:id", ctx -> employeeController.readOne(ctx, ctx.pathParam("id", Integer.class).get()));
        app.get("/employee/:id", ctx -> employeeController.readOne(ctx, Integer.parseInt(ctx.pathParam("id"))));
        app.post("/employee/:id/:first_name/:last_name/:phone", employeeController::create);

        app.start(8080);
    }
}
