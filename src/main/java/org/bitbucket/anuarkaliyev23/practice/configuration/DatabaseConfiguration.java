package org.bitbucket.anuarkaliyev23.practice.configuration;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.bitbucket.anuarkaliyev23.practice.model.Employee;

import java.sql.SQLException;
import java.util.Objects;

public class DatabaseConfiguration {
    private final String connectionString;
    private final ConnectionSource source;

    public DatabaseConfiguration(String connectionString) throws SQLException {
        this.connectionString = connectionString;
        source = new JdbcConnectionSource(connectionString);

    }

    public String getConnectionString() {
        return connectionString;
    }

    public ConnectionSource connectionSource() {
        return source;
    }

    public void createTables() throws SQLException {
        TableUtils.createTableIfNotExists(connectionSource(), Employee.class);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DatabaseConfiguration that = (DatabaseConfiguration) o;
        return Objects.equals(connectionString, that.connectionString);
    }

    @Override
    public int hashCode() {
        return Objects.hash(connectionString);
    }

    @Override
    public String toString() {
        return "DatabaseConfiguration{" +
                "connectionString='" + connectionString + '\'' +
                '}';
    }
}
