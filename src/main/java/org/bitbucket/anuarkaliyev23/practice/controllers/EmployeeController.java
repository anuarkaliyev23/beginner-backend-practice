package org.bitbucket.anuarkaliyev23.practice.controllers;

import com.j256.ormlite.dao.Dao;
import io.javalin.http.Context;
import org.bitbucket.anuarkaliyev23.practice.model.Employee;

import java.sql.SQLException;

public class EmployeeController {
    private final Dao<Employee, Integer> dao;

    public EmployeeController(Dao<Employee, Integer> dao) {
        this.dao = dao;
    }

    public void list(Context ctx) throws SQLException {
        ctx.result(dao.queryForAll().toString());
    }

    public void readOne(Context ctx, int id) throws SQLException {
        Employee employee = dao.queryForId(id);
        if (employee == null) {
            ctx.status(404);
        } else {
            ctx.result(employee.toString());
        }
    }

    public void create(Context ctx) throws SQLException {
        int id = ctx.pathParam("id", Integer.class).get();
        String firstName = ctx.pathParam("first_name");
        String lastName = ctx.pathParam("last_name");
        String phone = ctx.pathParam("phone");
        Employee employee = new Employee(id, firstName, lastName, phone);
        dao.create(employee);
    }
}
